//
//  Eyes.h
//  EyesSDK
//
//  Created by Anton Chuev on 4/3/18.
//  Copyright © 2018 Anton. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Eyes : NSObject

- (void)check;

@end
