//
//  EyesSDK.h
//  EyesSDK
//
//  Created by Anton Chuev on 4/3/18.
//  Copyright © 2018 Anton. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for EyesSDK.
FOUNDATION_EXPORT double EyesSDKVersionNumber;

//! Project version string for EyesSDK.
FOUNDATION_EXPORT const unsigned char EyesSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <EyesSDK/PublicHeader.h>

#import "EyesSDK/Eyes.h"


